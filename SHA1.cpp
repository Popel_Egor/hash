#include <iostream>
#include <cmath>

using namespace std;

unsigned int rlr(unsigned int x, int shift);
unsigned int F(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int G(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int H(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int I(unsigned int X, unsigned int Y, unsigned int Z);

int main()
{
	FILE *f = fopen("input.txt", "rb");
	fseek(f, 0, SEEK_END);
	unsigned int L = ftell(f);
	L *= 8;
	rewind(f);
	unsigned int L2;
	if (L % 512 >= 448)
		L2 = L + 512 + (512 - (L % 512));
	else
		L2 = L + (512 - (L % 512));
	unsigned int h0, h1, h2, h3, h4;
	h0 = 0x67452301; h1 = 0xefcdab89; h2 = 0x98badcfe; h3 = 0x10325476; h4 = 0xc3d2e1f0;
	
	int n = 1;
	while (n * 512 <= L2)
	{
		unsigned int A, B, C, D, E;
		A = h0; B = h1; C = h2; D = h3; E = h4;
		unsigned char X[64];
		if (n * 512 <= L)
			fread(X, 1, 64, f);
		else
		{
			if (n * 512 != L2)
			{
				fread(X, 1, (L % 512) / 8, f);
				X[(L % 512) / 8] = 0x80;
				for (int i = ((L % 512) / 8) + 1; i < 64; i++)
					X[i] = 0x00;
			}
			else
			{
				if (L % 512 < 448)
				{
					fread(X, 1, (L % 512) / 8, f);
					X[(L % 512) / 8] = 0x80;
					for (int i = ((L % 512) / 8) + 1; i < 56; i++)
						X[i] = 0x00;
					unsigned int Lt = L;
					X[63] = Lt % 256;
					Lt = Lt / 256;
					X[62] = Lt % 256;
					Lt = Lt / 256;
					X[61] = Lt % 256;
					Lt = Lt / 256;
					X[60] = Lt % 256;
					for (int i = 56; i < 60; i++)
						X[i] = 0x00;
				}
				else
				{
					for (int i = 0; i < 56; i++)
						X[i] = 0x00;
					unsigned int Lt = L;
					X[63] = Lt % 256;
					Lt = Lt / 256;
					X[62] = Lt % 256;
					Lt = Lt / 256;
					X[61] = Lt % 256;
					Lt = Lt / 256;
					X[60] = Lt % 256;
					for (int i = 56; i < 60; i++)
						X[i] = 0x00;
				}
			}
		}
		unsigned int XX[80];
		for (int i = 0; i < 64; i = i + 4)
			XX[i / 4] = X[i] * 256 * 256 * 256 + X[i + 1] * 256 * 256 + X[i + 2] * 256 + X[i + 3];
//			XX[i / 4] = X[i + 3] * 256 * 256 * 256 + X[i + 2] * 256 * 256 + X[i + 1] * 256 + X[i];

		for (int i = 16; i < 80; i++)
			XX[i] = rlr(XX[i - 3] ^ XX[i - 8] ^ XX[i - 14] ^ XX[i - 16], 1);

		unsigned int k;
		unsigned int f;
		for (int i = 0; i < 80; i++)
		{
			if (i < 20)
			{
				f = F(B, C, D);
				k = 0x5a827999;
			}
			else if (i < 40)
			{
				f = G(B, C, D);
				k = 0x6ed9eba1;
			}
			else if (i < 60)
			{
				f = H(B, C, D);
				k = 0x8f1bbcdc;
			}
			else
			{
				f = I(B, C, D);
				k = 0xca62c1d6;
			}
			unsigned int temp = rlr(A, 5) + f + E + k + XX[i];
			E = D;
			D = C;
			C = rlr(B, 30);
			B = A;
			A = temp;
		}

		h0 = h0 + A;
		h1 = h1 + B;
		h2 = h2 + C;
		h3 = h3 + D;
		h4 = h4 + E;

		n++;
	}
	fclose(f);
	unsigned char res[20];
	res[3] = h0 % 256;
	h0 = h0 / 256;
	res[2] = h0 % 256;
	h0 = h0 / 256;
	res[1] = h0 % 256;
	h0 = h0 / 256;
	res[0] = h0 % 256;
	res[7] = h1 % 256;
	h1 = h1 / 256;
	res[6] = h1 % 256;
	h1 = h1 / 256;
	res[5] = h1 % 256;
	h1 = h1 / 256;
	res[4] = h1 % 256;
	res[11] = h2 % 256;
	h2 = h2 / 256;
	res[10] = h2 % 256;
	h2 = h2 / 256;
	res[9] = h2 % 256;
	h2 = h2 / 256;
	res[8] = h2 % 256;
	res[15] = h3 % 256;
	h3 = h3 / 256;
	res[14] = h3 % 256;
	h3 = h3 / 256;
	res[13] = h3 % 256;
	h3 = h3 / 256;
	res[12] = h3 % 256;
	res[19] = h4 % 256;
	h4 = h4 / 256;
	res[18] = h4 % 256;
	h4 = h4 / 256;
	res[17] = h4 % 256;
	h4 = h4 / 256;
	res[16] = h4 % 256;
	f = fopen("output.bin", "wb");
	fwrite(res, 1, 20, f);
	fclose(f);
	return 0;
}

unsigned int rlr(unsigned int x, int shift)
{
	return (x << shift) | (x >> (32 - shift));
}

unsigned int F(unsigned int X, unsigned int Y, unsigned int Z)
{
	return (X & Y) | ((~X) & Z);
}

unsigned int G(unsigned int X, unsigned int Y, unsigned int Z)
{
	return X ^ Y ^ Z;
}

unsigned int H(unsigned int X, unsigned int Y, unsigned int Z)
{
	return (X & Y) | (X & Z) | (Y & Z);
}

unsigned int I(unsigned int X, unsigned int Y, unsigned int Z)
{
	return X ^ Y ^ Z;
}