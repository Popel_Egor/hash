#include <iostream>
#include <cmath>

using namespace std;

unsigned int rlr(unsigned int x, int shift);
unsigned int F(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int G(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int H(unsigned int X, unsigned int Y, unsigned int Z);
unsigned int I(unsigned int X, unsigned int Y, unsigned int Z);

int main()
{
	FILE *f = fopen("input.txt", "rb");
	fseek(f, 0, SEEK_END);
	unsigned int L = ftell(f);
	L *= 8;
	rewind(f);
	unsigned int L2;
	if (L % 512 >= 448)
		L2 = L + 512 + (512 - (L % 512));
	else
		L2 = L + (512 - (L % 512));
	unsigned int A, B, C, D;
	A = 0x67452301; B = 0xefcdab89; C = 0x98badcfe; D = 0x10325476;
	unsigned int T[64];
	for (int i = 0; i < 64; i++)
	{
		double temp = 4294967296 * fabs(sin(static_cast< double >(i + 1)));
//		double temp = 4294967296 * fabs(sin((double) i));
		T[i] = (unsigned int) temp;
	}
	int s[64];
	s[0] = 7; s[1] = 12; s[2] = 17; s[3] = 22;
	s[4] = 7; s[5] = 12; s[6] = 17; s[7] = 22;
	s[8] = 7; s[9] = 12; s[10] = 17; s[11] = 22;
	s[12] = 7; s[13] = 12; s[14] = 17; s[15] = 22;

	s[16] = 5; s[17] = 9; s[18] = 14; s[19] = 20;
	s[20] = 5; s[21] = 9; s[22] = 14; s[23] = 20;
	s[24] = 5; s[25] = 9; s[26] = 14; s[27] = 20;
	s[28] = 5; s[29] = 9; s[30] = 14; s[31] = 20;

	s[32] = 4; s[33] = 11; s[34] = 16; s[35] = 23;
	s[36] = 4; s[37] = 11; s[38] = 16; s[39] = 23;
	s[40] = 4; s[41] = 11; s[42] = 16; s[43] = 23;
	s[44] = 4; s[45] = 11; s[46] = 16; s[47] = 23;

	s[48] = 6; s[49] = 10; s[50] = 15; s[51] = 21;
	s[52] = 6; s[53] = 10; s[54] = 15; s[55] = 21;
	s[56] = 6; s[57] = 10; s[58] = 15; s[59] = 21;
	s[60] = 6; s[61] = 10; s[62] = 15; s[63] = 21;
	
	int n = 1;
	while (n * 512 <= L2)
	{
		unsigned int AA, BB, CC, DD;
		AA = A; BB = B; CC = C; DD = D;
		unsigned char X[64];
		if (n * 512 <= L)
			fread(X, 1, 64, f);
		else
		{
			if (n * 512 != L2)
			{
				fread(X, 1, (L % 512) / 8, f);
				X[(L % 512) / 8] = 0x80;
				for (int i = ((L % 512) / 8) + 1; i < 64; i++)
					X[i] = 0x00;
			}
			else
			{
				if (L % 512 < 448)
				{
					fread(X, 1, (L % 512) / 8, f);
					X[(L % 512) / 8] = 0x80;
					for (int i = ((L % 512) / 8) + 1; i < 56; i++)
						X[i] = 0x00;
					unsigned int Lt = L;
					X[56] = Lt % 256;
					Lt = Lt / 256;
					X[57] = Lt % 256;
					Lt = Lt / 256;
					X[58] = Lt % 256;
					Lt = Lt / 256;
					X[59] = Lt % 256;
					for (int i = 60; i < 64; i++)
						X[i] = 0x00;
				}
				else
				{
					for (int i = 0; i < 56; i++)
						X[i] = 0x00;
					unsigned int Lt = L;
					X[56] = Lt % 256;
					Lt = Lt / 256;
					X[57] = Lt % 256;
					Lt = Lt / 256;
					X[58] = Lt % 256;
					Lt = Lt / 256;
					X[59] = Lt % 256;
					for (int i = 60; i < 64; i++)
						X[i] = 0x00;
				}
			}
		}
		unsigned int XX[16];
		for (int i = 0; i < 64; i = i + 4)
//			XX[i / 4] = X[i] * 256 * 256 * 256 + X[i + 1] * 256 * 256 + X[i + 2] * 256 + X[i + 3];
			XX[i / 4] = X[i + 3] * 256 * 256 * 256 + X[i + 2] * 256 * 256 + X[i + 1] * 256 + X[i];
		/*cout << "N#" << n << endl;
		for (int i = 0; i < 16; i++)
			cout << (unsigned int)XX[i] << endl;*/
		bool ffaa = true;

		//����� 1
		for (int i = 0; i < 16; i++)
		{
			A = B + rlr(A + F(B, C, D) + XX[i] + T[i], s[i]);
			unsigned int TEMP = D;
			D = C;
			C = B;
			B = A;
			A = TEMP;
		}

		int k = 1;
		//����� 2
		for (int i = 16; i < 32; i++)
		{
			A = B + rlr(A + G(B, C, D) + XX[k] + T[i], s[i]);
			unsigned int TEMP = D;
			D = C;
			C = B;
			B = A;
			A = TEMP;
			k = (k + 5) % 16;
		}

		k = 5;
		//����� 3
		for (int i = 32; i < 48; i++)
		{
			A = B + rlr(A + H(B, C, D) + XX[k] + T[i], s[i]);
			unsigned int TEMP = D;
			D = C;
			C = B;
			B = A;
			A = TEMP;
			k = (k + 3) % 16;
		}

		k = 0;
		//����� 4
		for (int i = 48; i < 64; i++)
		{
			A = B + rlr(A + I(B, C, D) + XX[k] + T[i], s[i]);
			unsigned int TEMP = D;
			D = C;
			C = B;
			B = A;
			A = TEMP;
			k = (k + 7) % 16;
		}

		A = AA + A;
		B = BB + B;
		C = CC + C;
		D = DD + D;

		if (ffaa)
		{
			ffaa = false;
			FILE *f1 = fopen("tt.bin", "wb");
			for (int i = 0; i < 1; i++)
			{
				unsigned int AAA = A;
				unsigned char res1[4];
				res1[0] = AAA % 256;
				AAA = AAA / 256;
				res1[1] = AAA % 256;
				AAA = AAA / 256;
				res1[2] = AAA % 256;
				AAA = AAA / 256;
				res1[3] = AAA % 256;
				fwrite(res1, 1, 4, f1);
			}
			fclose(f1);
		}
		n++;
	}
	fclose(f);
	unsigned char res[16];
	res[0] = A % 256;
	A = A / 256;
	res[1] = A % 256;
	A = A / 256;
	res[2] = A % 256;
	A = A / 256;
	res[3] = A % 256;
	res[4] = B % 256;
	B = B / 256;
	res[5] = B % 256;
	B = B / 256;
	res[6] = B % 256;
	B = B / 256;
	res[7] = B % 256;
	res[8] = C % 256;
	C = C / 256;
	res[9] = C % 256;
	C = C / 256;
	res[10] = C % 256;
	C = C / 256;
	res[11] = C % 256;
	res[12] = D % 256;
	D = D / 256;
	res[13] = D % 256;
	D = D / 256;
	res[14] = D % 256;
	D = D / 256;
	res[15] = D % 256;
	f = fopen("output.bin", "wb");
	fwrite(res, 1, 16, f);
	fclose(f);
	return 0;
}

unsigned int rlr(unsigned int x, int shift)
{
	return (x << shift) | (x >> (32 - shift));
}

unsigned int F(unsigned int X, unsigned int Y, unsigned int Z)
{
	return (X & Y) | ((~X) & Z);
}

unsigned int G(unsigned int X, unsigned int Y, unsigned int Z)
{
	return (X & Z) | ((~Z) & Y);
}

unsigned int H(unsigned int X, unsigned int Y, unsigned int Z)
{
	return X ^ Y ^ Z;
}

unsigned int I(unsigned int X, unsigned int Y, unsigned int Z)
{
	return Y ^ ((~Z) | X);
}